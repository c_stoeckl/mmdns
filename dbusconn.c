#include <dbus/dbus.h>
#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <assert.h>
#include <ctype.h>

#include "hashtable.h"
#include "dbusconn.h"

#define SERVER_RUNNING 2;
int done = 0;

#define MDNS_PRINTER 0x33
#define DBUS_SERVICE 0x34

struct printer {
    char     label[256];
    char     name[256];
    char     type[128];
    uint32_t desc_length;
    uint8_t  description[1024];
};

struct service {
    char serv_browse[256];
};

ht_hash_table *printers; 
ht_hash_table *services;


static void check_and_abort(DBusError *error) {
	  
    if (dbus_error_is_set(error)) {
        puts(error->message);  
        abort(); 
    }
}

/* Read the first label from string *name, unescape "\" and write it to dest */
static char *unescape_label(const char *name, char *dest, size_t size) {
    unsigned i = 0;
    char *d;

    assert(dest);
    assert(size > 0);
    assert(name);

    d = dest;

    for (;;) {
        if (i >= size)
            return NULL;

        // if (*name == '.') {
        //     (name)++;
        //     break;
        // }

        if (*name == 0)
            break;

        if (*name == '\\') {
            /* Escaped character */

            (name) ++;

            if (*name == 0)
                /* Ending NUL */
                return NULL;

            else if (*name == '\\' || *name == '.') {
                /* Escaped backslash or dot */
                *(d++) = *((name) ++);
                i++;
            } else if (isdigit(*name)) {
                int n;

                /* Escaped literal ASCII character */

                if (!isdigit(*(name+1)) || !isdigit(*(name+2)))
                    return NULL;

                n = ((uint8_t) (*name - '0') * 100) + ((uint8_t) (*(name+1) - '0') * 10) 
                    + ((uint8_t) (*(name +2) - '0'));

                if (n > 255 || n == 0)
                    return NULL;

                *(d++) = (char) n;
                i++;

                (name) += 3;
            } else
                return NULL;

        } else {

            /* Normal character */

            *(d++) = *((name) ++);
            i++;
        }
    }

    assert(i < size);

    *d = 0;

    return dest;
}

static void send_signal_service_cache(struct dbus_client *client, char* path) {
    dbus_uint32_t serial;

    DBusMessage* msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.ServiceBrowser", // interface name of the signal
                                 "CacheExhausted"); // name of the signal

    //printf("service cache %s %s\n",client->serv,client->clnt);
    dbus_message_set_sender(msg,client->serv);
    dbus_message_set_destination(msg,client->clnt);
    
    // send the message and flush the connection
    dbus_connection_send(client->conn, msg, &serial);
    dbus_connection_flush(client->conn);
    
    // free the message and close the connection
    dbus_message_unref(msg);
}

static void send_signal_service_item(struct dbus_client *client, 
                                     char *path, char *name, char *type, int prot) {
    DBusMessage* msg;
    dbus_uint32_t serial;

    // create a signal & check for errors 
    msg = dbus_message_new_signal(path, // object name of the signal
                                  "org.freedesktop.Avahi.ServiceBrowser", // interface name of the signal
                                  "ItemNew"); // name of the signal

    dbus_message_set_sender(msg,client->serv);
    dbus_message_set_destination(msg,client->clnt);

   int iface = 2;
   char *domain = "local";
   uint32_t flags = 0x04;

   dbus_message_append_args(msg, 
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &name,
                DBUS_TYPE_STRING, &type,
                DBUS_TYPE_STRING, &domain,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

   // send the message and flush the connection
   dbus_connection_send(client->conn, msg, &serial);
   dbus_connection_flush(client->conn);
   
   printf("Signal Service Item Sent %s %s %s %i\n",path,name,type,prot);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static void send_signal_service_all(struct dbus_client *client, char* path) {
    DBusMessage* msg;
    dbus_uint32_t serial;

    // create a signal & check for errors 
    msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.ServiceBrowser", // interface name of the signal
                                 "AllForNow"); // name of the signal

    dbus_message_set_sender(msg,client->serv);
    dbus_message_set_destination(msg,client->clnt);
   
    // send the message and flush the connection
    dbus_connection_send(client->conn, msg, &serial);
    dbus_connection_flush(client->conn);
    
    // free the message and close the connection
    dbus_message_unref(msg);
}

static void send_signal_record_cache(struct dbus_client *client, char* path) {
   DBusMessage* msg;
   dbus_uint32_t serial;

   // create a signal & check for errors 
   msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.RecordBrowser", // interface name of the signal
                                 "CacheExhausted"); // name of the signal

    dbus_message_set_sender(msg,client->serv);
    dbus_message_set_destination(msg,client->clnt);
   
   // send the message and flush the connection
   dbus_connection_send(client->conn, msg, &serial);
   dbus_connection_flush(client->conn);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static void send_signal_record_item(struct dbus_client *client, char * path, 
                                    struct printer *print, int prot) {
   DBusMessage* msg;
   dbus_uint32_t serial;

    // create a signal & check for errors 
    msg = dbus_message_new_signal(path, // object name of the signal
                                  "org.freedesktop.Avahi.RecordBrowser", // interface name of the signal
                                  "ItemNew"); // name of the signal

    dbus_message_set_sender(msg,client->serv);
    dbus_message_set_destination(msg,client->clnt);

    int iface = 2;
    int p = prot;
    char *n  = print->label;
    uint16_t clazz = 1;
    uint16_t type = 16;
    uint8_t *d = print->description;
    uint32_t s = print->desc_length; 
    uint32_t flags = 0x05;

    dbus_message_append_args(msg, 
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &p,
                DBUS_TYPE_STRING, &n,
                DBUS_TYPE_UINT16, &clazz,
                DBUS_TYPE_UINT16, &type,
                DBUS_TYPE_ARRAY, DBUS_TYPE_BYTE, &d, s,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

    // send the message and flush the connection
    dbus_connection_send(client->conn, msg, &serial);
    dbus_connection_flush(client->conn);
   
    printf("signal record %s %s %i\n",print->name,print->label,prot);

   // cleanup hash table
   //if (it) ht_delete(printers,name);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}


static void respond_to_introspect(DBusConnection *connection, DBusMessage *request) {
	  
    DBusMessage *reply;
    const char *introspection_data =
        " <!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\" "
        "\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">"
        " <!-- dbus-sharp 0.8.1 -->"
        " <node>"
        "   <interface name=\"org.freedesktop.DBus.Introspectable\">"
        "     <method name=\"Introspect\">"
        "       <arg name=\"data\" direction=\"out\" type=\"s\" />"
        "     </method>"
        "   </interface>"
        "   <interface name=\"org.freedesktop.Avahi.Server\">"
        "     <method name=\"GetAPIVersion\">"
        "       <arg name=\"version\" type=\"u\" direction=\"out\"/>"
        "     </method>"
        "     <method name=\"GetState\">"
        "       <arg name=\"version\" type=\"i\" direction=\"out\"/>"
        "     </method>"
        "     <method name=\"ServiceBrowserNew\">"
        "       <arg name=\"interface\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"protocol\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"type\" type=\"s\" direction=\"in\"/>"
        "       <arg name=\"domain\" type=\"s\" direction=\"in\"/>"
        "       <arg name=\"flags\" type=\"u\" direction=\"in\"/>"
        "       <arg name=\"path\" type=\"o\" direction=\"out\"/>"
        "     </method>"
        "     <method name=\"RecordBrowserNew\">"
        "       <arg name=\"interface\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"protocol\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"name\" type=\"s\" direction=\"in\"/>"
        "       <arg name=\"class\" type=\"q\" direction=\"in\"/>"
        "       <arg name=\"type\" type=\"q\" direction=\"in\"/>"
        "       <arg name=\"flags\" type=\"u\" direction=\"in\"/>"
        "       <arg name=\"path\" type=\"o\" direction=\"out\"/>"
        "     </method>"
        "   </interface>"
        "   <interface name=\"org.freedesktop.Avahi.RecordBrowser\">"
        "     <method name=\"Free\">"
        "   </interface>"
        " </node>";
    
    reply = dbus_message_new_method_return(request);
        
    dbus_message_append_args(reply, DBUS_TYPE_STRING, &introspection_data,				   
                             DBUS_TYPE_INVALID);
        
    dbus_connection_send(connection, reply, NULL);
        
    dbus_message_unref(reply);
}

static void respond_to_api(DBusConnection *connection, DBusMessage *request) {
    uint32_t ret = 0x203;
     
    DBusMessage *reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_UINT32, &ret,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}

static void respond_to_state(DBusConnection *connection, DBusMessage *request) {
    int32_t ret = SERVER_RUNNING;
     
    DBusMessage *reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_INT32, &ret,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}

static void respond_to_service(DBusConnection *connection, DBusMessage *request, 
                               struct dbus_conn *srv) {
    DBusMessage *reply;
    DBusError error;  
 
    int32_t iface,prot;
    const char *type, *domain;
    uint32_t flags;

    dbus_error_init(&error);    
    dbus_message_get_args(request, &error,
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &type,
                DBUS_TYPE_STRING, &domain,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

    if (dbus_error_is_set(&error)) {
        reply = dbus_message_new_error(request, "wrong_arguments", "Illegal arguments");
        dbus_connection_send(connection, reply, NULL);
        dbus_message_unref(reply);
        return;
    }

    printf("service %i,%i,%s,%s,%u\n",iface,prot,type,domain,flags);

    char path[256];
    sprintf(path,"/Client0/ServiceBrowser%i",srv->services++);
    char *p = path;

    // first connection from client
    if (srv->client == NULL) {
        srv->client = malloc(sizeof(struct dbus_client));
        srv->client->conn = srv->conn;
        strcpy(srv->client->serv,dbus_message_get_destination(request));
        strcpy(srv->client->clnt,dbus_message_get_sender(request));
    }

    printf("service %s %s %s \n",srv->client->serv,srv->client->clnt,path);
     
    reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_OBJECT_PATH , &p,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);

    strcpy(srv->query->name,type);
    strcat(srv->query->name,".local");
    ht_insert(services,srv->query->name,DBUS_SERVICE,256,&path);

    // send queries through mmdnsd
    write(srv->dbus_pipe[1], ".", 1); 
}

static void respond_to_record(DBusConnection *connection, DBusMessage *request,
                              struct dbus_conn *srv) {
    DBusError error;  
 
    int32_t iface,prot;
    const char *name;
    uint16_t clazz,type;
    uint32_t flags;

    dbus_error_init(&error);    
    dbus_message_get_args(request, &error,
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &name,
                DBUS_TYPE_UINT16, &clazz,
                DBUS_TYPE_UINT16, &type,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

    if (dbus_error_is_set(&error)) {
        DBusMessage *reply = dbus_message_new_error(request, "wrong_arguments", "Illegal arguments");
        dbus_connection_send(connection, reply, NULL);
        dbus_message_unref(reply);
        return;
    }

    printf("record %i,%i,%s,%i,%i,%u\n",iface,prot,name,clazz,type,flags);


    struct dbus_client *client = malloc(sizeof(struct dbus_client));
    client->conn = connection;
    strcpy(client->serv,dbus_message_get_destination(request));
    strcpy(client->clnt,dbus_message_get_sender(request));

    char label[256];
    unescape_label(name,label,strlen(name));

    char path[256];
    sprintf(path,"/Client0/RecordBrowser%i",srv->records++);
    printf("record %s %s %s %s \n",client->serv,client->clnt,label,path);
    char *p = path;

    DBusMessage *reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_OBJECT_PATH , &p,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);

    // if information exists send
    ht_item *it = ht_search(printers,label);
	if (it) {
        struct printer *print = (struct printer*) it->value;
        strcpy(print->label,name);
        strcpy(print->name,label);
        printf("record found %s %s %i\n",print->label,print->name,print->desc_length); 
        send_signal_record_item(client,path,print,0);
        send_signal_record_item(client,path,print,1);
    } 

    send_signal_record_cache(client,path);

    free(client);
} 

static void respond_to_free(DBusConnection *connection, DBusMessage *request) {
     
    DBusMessage *reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}


static DBusHandlerResult dbus_messages(DBusConnection *connection, DBusMessage *message, 
                                           void *user_data) {
	  
    const char *interface_name = dbus_message_get_interface(message);	  
    const char *member_name = dbus_message_get_member(message);

    struct dbus_conn * srv = (struct dbus_conn*) user_data;

    printf("handler: %s %s %u \n",interface_name,member_name, srv->client);

    if (interface_name == NULL) 
        return DBUS_HANDLER_RESULT_HANDLED;	

    if      (strcmp("org.freedesktop.DBus.Introspectable", interface_name)==0 &&	      
             strcmp("Introspect", member_name)==0) {
        respond_to_introspect(connection, message);	  
        return DBUS_HANDLER_RESULT_HANDLED;	  
    } else if (0==strcmp("org.freedesktop.Avahi.Server", interface_name)) {	     
        if (strcmp("GetAPIVersion", member_name) == 0) {
            if (srv->client && srv->working == 0) {
                free(srv->client); 
                srv->client = NULL;
            }
            respond_to_api(connection, message);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp("GetState", member_name) == 0) {
            respond_to_state(connection, message);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp("ServiceBrowserNew", member_name) == 0) {
            respond_to_service(connection, message, srv);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp("RecordBrowserNew", member_name) == 0) {
            respond_to_record(connection, message, srv);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
    } else if (strcmp(interface_name,"org.freedesktop.Avahi.RecordBrowser")==0 &&
               strcmp(member_name,"Free")==0) {	
        respond_to_free(connection, message);
        return DBUS_HANDLER_RESULT_HANDLED;
    } else {	  
        DBUS_HANDLER_RESULT_NOT_YET_HANDLED;	
    }
}

static int dbus_loop(struct dbus_conn *srv) {

    char buf[256];

    fd_set sockfd_set;
    while(1) {		  
        dbus_connection_read_write_dispatch(srv->conn, 100); 

        // slow down operations to let network catch up
        struct timespec tr, tw = {0, 10000000}; //10 ms
        nanosleep(&tw,&tr);
    }

    return 0;
}

struct dbus_conn* dbus_connect() {
    pthread_t tid;
	pthread_attr_t attr;

 	struct dbus_conn *server = malloc(sizeof(struct dbus_conn));
    memset(server, 0, sizeof(struct dbus_conn));

    //initialize data structures
    server->query  = malloc(sizeof(struct dbus_query));
    server->client = NULL; 

    // make maps for service browsers and printers
    services = ht_new();
    printers = ht_new();

    // initialize counters
    server->working  = 0;
    server->services = 0;
    server->records  = 0; 

    server->err  = malloc(sizeof(DBusError));

    dbus_error_init(server->err);  
    server->conn = dbus_bus_get(DBUS_BUS_SYSTEM, server->err); 
    check_and_abort(server->err);

    dbus_bus_request_name(server->conn, "org.freedesktop.Avahi", 
                          DBUS_NAME_FLAG_REPLACE_EXISTING, server->err);	  
    check_and_abort(server->err);

    dbus_connection_add_filter(server->conn,dbus_messages,server,NULL);

	if (pipe(server->dbus_pipe) != 0) {
		perror("pipe()\n");
		free(server);
		return NULL;
	} 

	// init thread
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	if (pthread_create(&tid, &attr, (void *(*)(void *)) dbus_loop, (void *) server) != 0) {
		free(server);
		return NULL;
	}

	return server;
}


static char * extract_type(char *t, char *l) {
    char *f = strstr(l,".local");
    if (f==NULL) return f;
    
    int i=f-l;
    strncpy(t,l,i);
    t[i] = 0; //0 terminate string

    return t;
}

static char * extract_ltype(char *s) {

    char *f = strstr(s,"._tcp.local");

    //search for next _ in string
    for (f; f>s; f--) {
        if (*f == '_') break;
    }
    //printf("extract type %s\n",f);
    return f;
}

static char * extract_name(char *n, char *h, char* type) {

    char *f = strstr(h,type);
    if (f==NULL) return f;

    //copy string to type substring
    int i = f-h-1;  
    strncpy(n,h,i);
    n[i] = 0; //0 terminate string
    
    return n;
}

int dbus_process_query(struct dbus_conn * srv, struct mdns_pkt *pkt) {

    if (pkt == NULL) return 0;

    //check all answer records
    if (pkt->num_ans_rr > 0) {
        struct rr_list *next = pkt->rr_ans;

        struct printer *print=malloc(sizeof(struct printer));
        do {
            if (next == NULL) break;
            printf("dbus ipp %x %s \n",next->e->type,next->e->hname); 
            
            if (next->e->type == RR_TXT) {
                char *ltype = extract_ltype(next->e->hname);
                ht_item *it = ht_search(services,ltype);
                if (it) {
                    printf("dbus browser %s %s \n",ltype,(char*) it->value);
                    extract_name(print->name,next->e->hname,ltype);
                    extract_type(print->type,ltype);
                    printf("dbus printer name %s|%s|%s \n",print->name,print->type,next->e->hname);
                    print->desc_length = next->e->data.TXT.length;
                    memcpy(print->description,next->e->data.TXT.data,print->desc_length);
                    ht_insert(printers,next->e->hname,MDNS_PRINTER,sizeof(struct printer),print);
                    send_signal_service_item(srv->client, (char*) it->value, print->name,
                                             print->type,0);
                }
            }
            next = next->next;
        } while(1);

        free(print);
    }
}

int dbus_process_done(struct dbus_conn * srv) {

    char buf[256];
    printf("dbus_process_done %s %s \n",srv->client->serv,srv->client->clnt);

    //send finish for all browsers
    for (int i=0; i<srv->services; i++) {
        sprintf(buf,"/Client0/ServiceBrowser%i",i);
        send_signal_service_cache(srv->client,buf);
        send_signal_service_all(srv->client,buf);
          
    }
    srv->services = 0;
    srv->records  = 0;
}
