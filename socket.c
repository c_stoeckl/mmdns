#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include "hash_table.h"

char *socket_path = "/var/run/avahi-daemon/socket";

static int open_socket(char* path) {
  struct sockaddr_un addr;
  int fd;

  umask(0); //make world readable

  if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("socket error");
    return -1;
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  if (*path == '\0') {
    *addr.sun_path = '\0';
    strncpy(addr.sun_path+1, path+1, sizeof(addr.sun_path)-2);
  } else {
    strncpy(addr.sun_path, path, sizeof(addr.sun_path)-1);
    unlink(path);
  }

  if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("bind error");
    return -1;
  }

  if (listen(fd, 5) == -1) {
    perror("listen error");
    return -1;
  }

  return fd; 
}

int main(int argc, char *argv[]) {

  char inbuf[256];
  char outbuf[256];
  int fd,cl,rc;

  ht_hash_table* ht4 = ht_new();
  ht_insert(ht4, "blue.local", "192.168.1.104");
  ht_insert(ht4, "linux.local","192.168.1.10");
  ht_insert(ht4, "orange.local","192.168.1.20");
  ht_insert(ht4, "birchvalley.local","192.168.1.100");
  
  if (argc > 1) socket_path=argv[1];

  fd = open_socket(socket_path);

  if (fd < 0) {
    perror("no socket");
    exit(1);
  }

  while (1) {
    if ( (cl = accept(fd, NULL, NULL)) == -1) {
      perror("accept error");
      continue;
    }

    printf("accepted\n");

    while ( (rc=read(cl,inbuf,sizeof(inbuf))) > 0) {
      printf("read %u bytes: %.*s\n", rc, rc, inbuf);

      inbuf[rc-1] = 0;
      if (strstr(inbuf,"IPV4") != NULL) {
        char* name = strchr(inbuf,'4') + 2;
        sprintf(outbuf,"+ 2 0 %s %s\n",name,ht_search(ht4,name));
      } else if (strstr(inbuf,"IPV6") != NULL) {
        sprintf(outbuf,"+ 2 1 blue.local fe80::ba27:ebff:fee9:3557\n");
      } else {
        sprintf(outbuf,"+ 2 0 blue.local 192.168.1.104\n"); 
      }
      rc = write(cl,outbuf,strlen(outbuf));
      
    }
    if (rc == -1) {
      perror("read");
      exit(-1);
    }
    else if (rc == 0) {
      printf("EOF\n");
      // close(cl);
      // return 0;
    }
  }
 
  return 0;
}
