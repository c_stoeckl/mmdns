#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <stdlib.h>
#include <pthread.h>


#include "mdnsd.h"
#include "hashtable.h"
#include "nsssocket.h"


struct ht_A {
	int ttl;
	char ip[INET_ADDRSTRLEN];
};

struct ht_AAAA {
	int ttl;
	char ip[INET6_ADDRSTRLEN];
};

ht_hash_table *ht4;
ht_hash_table *ht6;


static int open_socket(char* path) {
  struct sockaddr_un addr;
  int fd;

  umask(0); //make world readable

  if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("socket error");
    return -1;
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  if (*path == '\0') {
    *addr.sun_path = '\0';
    strncpy(addr.sun_path+1, path+1, sizeof(addr.sun_path)-2);
  } else {
    strncpy(addr.sun_path, path, sizeof(addr.sun_path)-1);
    unlink(path);
  }

  if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("bind error");
    return -1;
  }

  if (listen(fd, 5) == -1) {
    perror("listen error");
    return -1;
  }

  return fd; 
}

static int nss_socket_loop(struct nsss *svr) {

  char inbuf[256];
  char outbuf[256];
  int fd,cl,rc;

  fd = open_socket(SOCKET_PATH);

  if (fd < 0) {
    perror("no socket");
    exit(1);
  }

  printf("socket set up\n");

  char buf[256];


	fd_set sockfd_set;
  while (1) {

    if ( (cl = accept(fd, NULL, NULL)) == -1) {
      perror("accept error");
      continue;
    }

    printf("accepted\n");
    
    do {
      do { // read everything in socket
        rc=read(cl,inbuf,sizeof(inbuf));
      } while (rc == sizeof(inbuf));

      if (rc == -1) {
        perror("read");
        exit(-1);
      }
      else if (rc == 0) {
        printf("EOF\n");
        break;
      }

      inbuf[rc-1] = 0;
      printf("read %u bytes: %.*s|\n", rc, rc, inbuf);

      if (strstr(inbuf,"RESOLVE-HOSTNAME-IPV4") != NULL) {
        struct nss_query *query = malloc(sizeof(struct nss_query));
        strcpy(query->name,(char*) (strchr(inbuf,'4') + 2));
        query->type = RR_A;
        svr->query = query;

        //notify server
        write(svr->nss_pipe[1], ".", 1);   

        //wait for answer
        struct timeval tv = {1, 0};   
        FD_ZERO(&sockfd_set);
        FD_SET(svr->ip_pipe[0], &sockfd_set);
        select(svr->ip_pipe[0]+1, &sockfd_set, NULL, NULL, &tv);

        if (FD_ISSET(svr->ip_pipe[0], &sockfd_set)) {
          read(svr->ip_pipe[0],buf,1);	//clear pipe 
          sprintf(outbuf,"+ 2 0 %s %s\n",svr->query->name,svr->query->ans);
        } else {
          sprintf(outbuf,"+ 2 0 %s %s\n",svr->query->name,"");
        } 
        free(svr->query);
        svr->query=NULL;
      } else if (strstr(inbuf,"RESOLVE-HOSTNAME-IPV6") != NULL) {
        struct nss_query *query = malloc(sizeof(struct nss_query));
        strcpy(query->name,(char*) (strchr(inbuf,'6') + 2));
        query->type = RR_AAAA;
        svr->query = query;

        //notify server
        write(svr->nss_pipe[1], ".", 1);  

        //wait for answer
        struct timeval tv = {1, 0};   
        FD_ZERO(&sockfd_set);
        FD_SET(svr->ip_pipe[0], &sockfd_set);
        select(svr->ip_pipe[0]+1, &sockfd_set, NULL, NULL, &tv);

        if (FD_ISSET(svr->ip_pipe[0], &sockfd_set)) {
          read(svr->ip_pipe[0],buf,1);	//clear pipe 
          sprintf(outbuf,"+ 2 1 %s %s\n",svr->query->name,svr->query->ans);
        } else {
          sprintf(outbuf,"+ 2 1 %s %s\n",svr->query->name,"");
        } 
        free(svr->query);
        svr->query=NULL;
      } else {
        sprintf(outbuf,"+ 2 0 \n"); 
      }
      write(cl,outbuf,strlen(outbuf)); 
    
    } while (1);
  }
 
  return 0;
}

struct nsss* nss_socket() {
  pthread_t tid;
	pthread_attr_t attr;

	struct nsss *server = malloc(sizeof(struct nsss));
	memset(server, 0, sizeof(struct nsss));

  //init hash tables
  ht4 = ht_new();
	ht6 = ht_new();

	if (pipe(server->nss_pipe) != 0) {
		perror("pipe()\n");
		free(server);
		return NULL;
	} 

	if (pipe(server->ip_pipe) != 0) {
		perror("pipe()\n");
		free(server);
		return NULL;
	}

	// init thread
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	if (pthread_create(&tid, &attr, (void *(*)(void *)) nss_socket_loop, (void *) server) != 0) {
		free(server);
		return NULL;
	}

	return server;
}

// processes the MDNS reply type A packet
static int nss_process_mdns_reply_A_pkt(struct mdnsd *svr, struct mdns_pkt *pkt) {
	
	struct in_addr inet;
	inet.s_addr = (in_addr_t) pkt->rr_ans->e->data.A.addr;

	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC,&t);

	struct ht_A a;
	a.ttl = pkt->rr_ans->e->ttl+t.tv_sec;
	inet_ntop(AF_INET,&inet,a.ip,INET_ADDRSTRLEN);	

	DEBUG_PRINTF("reply_a %s %s\n",pkt->rr_ans->e->hname,a.ip);

	ht_insert(ht4,pkt->rr_ans->e->hname,RR_A,sizeof(struct ht_A),&a);
	return 0;
}

// processes the MDNS reply type AAAA packet
static int nss_process_mdns_reply_AAAA_pkt(struct mdnsd *svr, struct mdns_pkt *pkt) {
	
	struct in6_addr inet6;
	inet6 = pkt->rr_ans->e->data.AAAA.addr;
	
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC,&t);

	struct ht_AAAA a;
	a.ttl = pkt->rr_ans->e->ttl+t.tv_sec;
	inet_ntop(AF_INET6, &inet6, a.ip, INET6_ADDRSTRLEN);	

	DEBUG_PRINTF("reply_AAAA %s %s\n",pkt->rr_ans->e->hname,a.ip);

	ht_insert(ht6,pkt->rr_ans->e->hname,RR_AAAA,sizeof(struct ht_AAAA),&a);
	return 0;
}

int nss_process_query(struct mdnsd *svr, struct nsss* nss, struct mdns_pkt *pkt) { 
 
  if (nss == NULL) {
    return 0;
  }

  // process packet
  if (pkt != NULL) {
    if (pkt->rr_ans->e->type == RR_A) {
						nss_process_mdns_reply_A_pkt(svr, pkt);
		}
		if (pkt->rr_ans->e->type == RR_AAAA) {
						nss_process_mdns_reply_AAAA_pkt(svr, pkt);
		}
  }

	ht_item *it=NULL;
	int ttl = 0;
	if (nss->query->type == RR_A) {
		it = ht_search(ht4,nss->query->name);
		if (it) {
			strcpy(nss->query->ans,((struct ht_A*) it->value)->ip);
			ttl = ((struct ht_A*) it->value)->ttl;
		}	
	}

	if (nss->query->type == RR_AAAA) {
		it = ht_search(ht6,nss->query->name);
		if (it) {
			strcpy(nss->query->ans,((struct ht_AAAA*) it->value)->ip);
			ttl = ((struct ht_AAAA*) it->value)->ttl;
		}		
	}

	//return value from cache only if time < ttl
	if (it) {
		struct timespec t;
		clock_gettime(CLOCK_MONOTONIC,&t);
		if (t.tv_sec < ttl) {
			printf("nss_query %s %s \n",nss->query->name,nss->query->ans);
			write(nss->ip_pipe[1],".",1);
			return 0;	
		}
	}

	return 1;
}	
