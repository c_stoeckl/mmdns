#include <dbus-1.0/dbus/dbus.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "message.h"

#define SERVER_RUNNING 2;

int serv = 1;
int done = 0;
const char * sender;
const char * dest;

static void check_and_abort(DBusError *error);
static DBusHandlerResult tutorial_messages(DBusConnection *connection, DBusMessage *message, void *user_data);
static void respond_to_introspect(DBusConnection *connection, DBusMessage *request);
static void respond_to_api(DBusConnection *connection, DBusMessage *request);
static void respond_to_state(DBusConnection *connection, DBusMessage *request);
static void respond_to_service(DBusConnection *connection, DBusMessage *request);
static void respond_to_record(DBusConnection *connection, DBusMessage *request);

static void check_and_abort(DBusError *error) {
	  
    if (dbus_error_is_set(error)) {
        puts(error->message);  
        abort(); 
    }
}

static void send_signal_service_cache(DBusConnection *conn, const char *send, const char* dest, char* path) {
   DBusMessage* msg;
   DBusMessageIter args;
   DBusError err;
   int ret;
   dbus_uint32_t serial = 0;

   // initialise the error value
   dbus_error_init(&err);

   // create a signal & check for errors 
   msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.ServiceBrowser", // interface name of the signal
                                 "CacheExhausted"); // name of the signal

    dbus_message_set_sender(msg,dest);
    dbus_message_set_destination(msg,send);
   
   // send the message and flush the connection
   dbus_connection_send(conn, msg, &serial);
   dbus_connection_flush(conn);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static void send_signal_service_item(DBusConnection *conn, const char *send, const char* dest, 
                             char * path, int prot) {
   DBusMessage* msg;
   DBusMessageIter args;
   DBusError err;
   int ret;
   dbus_uint32_t serial = 0;

   // initialise the error value
   dbus_error_init(&err);

   // create a signal & check for errors 
   msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.ServiceBrowser", // interface name of the signal
                                 "ItemNew"); // name of the signal

   dbus_message_set_sender(msg,dest);
   dbus_message_set_destination(msg,send);

   int iface = 2;
   char *name = "Epson";
   char *type = "_ipp._tcp";
   char *domain = "local";
   uint32_t flags = 0x04;

   dbus_message_append_args(msg, 
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &name,
                DBUS_TYPE_STRING, &type,
                DBUS_TYPE_STRING, &domain,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

   
   // send the message and flush the connection
   dbus_connection_send(conn, msg, &serial);
   dbus_connection_flush(conn);
   
   printf("Signal Service Item Sent %i\n",prot);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static void send_signal_service_all(DBusConnection *conn, const char *send, const char* dest, char* path) {
   DBusMessage* msg;
   DBusMessageIter args;
   DBusError err;
   int ret;
   dbus_uint32_t serial = 0;

   // initialise the error value
   dbus_error_init(&err);

   // create a signal & check for errors 
   msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.ServiceBrowser", // interface name of the signal
                                 "AllForNow"); // name of the signal

   dbus_message_set_sender(msg,dest);
   dbus_message_set_destination(msg,send);
   
   // send the message and flush the connection
   dbus_connection_send(conn, msg, &serial);
   dbus_connection_flush(conn);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static void send_signal_record_cache(DBusConnection *conn, DBusMessage *req, char* path) {
   DBusMessage* msg;
   DBusMessageIter args;
   DBusError err;
   int ret;
   dbus_uint32_t serial = 0;

   // initialise the error value
   dbus_error_init(&err);

   // create a signal & check for errors 
   msg = dbus_message_new_signal(path, // object name of the signal
                                 "org.freedesktop.Avahi.RecordBrowser", // interface name of the signal
                                 "CacheExhausted"); // name of the signal

    const char * send = dbus_message_get_sender(req);
    const char * dest = dbus_message_get_destination(req);
    dbus_message_set_sender(msg,dest);
    dbus_message_set_destination(msg,send);
   
   // send the message and flush the connection
   dbus_connection_send(conn, msg, &serial);
   dbus_connection_flush(conn);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static void send_signal_record_item(DBusConnection *conn, DBusMessage *req, char * path, int prot) {
   DBusMessage* msg;
   DBusMessageIter args;
   DBusError err;
   int ret;
   dbus_uint32_t serial = 0;

    // initialise the error value
    dbus_error_init(&err);

    // create a signal & check for errors 
    msg = dbus_message_new_signal(path, // object name of the signal
                                    "org.freedesktop.Avahi.RecordBrowser", // interface name of the signal
                                    "ItemNew"); // name of the signal

    const char * send = dbus_message_get_sender(req);
    const char * dest = dbus_message_get_destination(req);
    dbus_message_set_sender(msg,dest);
    dbus_message_set_destination(msg,send);

    int iface = 2;
    char *name = "Epson._ipp._tcp.local";
  
    uint16_t clazz = 1;
    uint16_t type = 16;

    uint8_t data[] = {1,2,3,4};
    uint32_t size = 467;
    uint8_t *d = message;

    uint32_t flags = 0x05;

    dbus_message_append_args(msg, 
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &name,
                DBUS_TYPE_UINT16, &clazz,
                DBUS_TYPE_UINT16, &type,
                DBUS_TYPE_ARRAY, DBUS_TYPE_BYTE, &d, size,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

   
   // send the message and flush the connection
   dbus_connection_send(conn, msg, &serial);
   dbus_connection_flush(conn);
   
   printf("Signal Record Item Sent %i\n",prot);
   
   // free the message and close the connection
   dbus_message_unref(msg);
}

static DBusHandlerResult tutorial_messages(DBusConnection *connection, DBusMessage *message, 
                                           void *user_data) {
	  
    const char *interface_name = dbus_message_get_interface(message);	  
    const char *member_name = dbus_message_get_member(message);

    printf("handler: %s %s \n",interface_name,member_name);

    if (0==strcmp("org.freedesktop.DBus.Introspectable", interface_name) &&	      
        0==strcmp("Introspect", member_name)) {

        respond_to_introspect(connection, message);	  
        return DBUS_HANDLER_RESULT_HANDLED;	  
    } else if (0==strcmp("org.freedesktop.Avahi.Server", interface_name)) {	     
        if (strcmp("GetAPIVersion", member_name) == 0) {
            respond_to_api(connection, message);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp("GetState", member_name) == 0) {
            respond_to_state(connection, message);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp("ServiceBrowserNew", member_name) == 0) {
            respond_to_service(connection, message);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp("RecordBrowserNew", member_name) == 0) {
            respond_to_record(connection, message);		  
            return DBUS_HANDLER_RESULT_HANDLED;
        }
    } else if (0==strcmp("org.freedesktop.Avahi.RecordBrowser", interface_name)) {	 
            return DBUS_HANDLER_RESULT_HANDLED;
    } else {	  
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;	  
    }
}

static void respond_to_introspect(DBusConnection *connection, DBusMessage *request) {
	  
    DBusMessage *reply;
    const char *introspection_data =
        " <!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\" "
        "\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">"
        " <!-- dbus-sharp 0.8.1 -->"
        " <node>"
        "   <interface name=\"org.freedesktop.DBus.Introspectable\">"
        "     <method name=\"Introspect\">"
        "       <arg name=\"data\" direction=\"out\" type=\"s\" />"
        "     </method>"
        "   </interface>"
        "   <interface name=\"org.freedesktop.Avahi.Server\">"
        "     <method name=\"GetAPIVersion\">"
        "       <arg name=\"version\" type=\"u\" direction=\"out\"/>"
        "     </method>"
        "     <method name=\"GetState\">"
        "       <arg name=\"version\" type=\"i\" direction=\"out\"/>"
        "     </method>"
        "     <method name=\"ServiceBrowserNew\">"
        "       <arg name=\"interface\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"protocol\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"type\" type=\"s\" direction=\"in\"/>"
        "       <arg name=\"domain\" type=\"s\" direction=\"in\"/>"
        "       <arg name=\"flags\" type=\"u\" direction=\"in\"/>"
        "       <arg name=\"path\" type=\"o\" direction=\"out\"/>"
        "     </method>"
        "     <method name=\"RecordBrowserNew\">"
        "       <arg name=\"interface\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"protocol\" type=\"i\" direction=\"in\"/>"
        "       <arg name=\"name\" type=\"s\" direction=\"in\"/>"
        "       <arg name=\"class\" type=\"q\" direction=\"in\"/>"
        "       <arg name=\"type\" type=\"q\" direction=\"in\"/>"
        "       <arg name=\"flags\" type=\"u\" direction=\"in\"/>"
        "       <arg name=\"path\" type=\"o\" direction=\"out\"/>"
        "     </method>"
        "   </interface>"
        "   <interface name=\"org.freedesktop.Avahi.RecordBrowser\">"
        "     <method name=\"Free\">"
        " </node>";
    
    reply = dbus_message_new_method_return(request);
        
    dbus_message_append_args(reply, DBUS_TYPE_STRING, &introspection_data,				   
                             DBUS_TYPE_INVALID);
        
    dbus_connection_send(connection, reply, NULL);
        
    dbus_message_unref(reply);
}

static void respond_to_api(DBusConnection *connection, DBusMessage *request) {
    DBusMessage *reply;
    uint32_t ret = 0x203;
     
    reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_UINT32, &ret,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}

static void respond_to_state(DBusConnection *connection, DBusMessage *request) {
    DBusMessage *reply;
    int32_t ret = SERVER_RUNNING;
     
    reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_INT32, &ret,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}

static void respond_to_service(DBusConnection *connection, DBusMessage *request) {
    DBusMessage *reply;
    DBusError error;  
 
    int32_t iface,prot;
    const char *type, *domain;
    uint32_t flags;

    printf("service\n");

    dbus_error_init(&error);    
    dbus_message_get_args(request, &error,
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &type,
                DBUS_TYPE_STRING, &domain,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

    if (dbus_error_is_set(&error)) {
        reply = dbus_message_new_error(request, "wrong_arguments", "Illegal arguments");
        dbus_connection_send(connection, reply, NULL);
        dbus_message_unref(reply);
        return;
    }

    printf("service %i,%i,%s,%s,%u\n",iface,prot,type,domain,flags);

    sender = dbus_message_get_sender(request);
    dest   = dbus_message_get_destination(request);

    char path[256];
    sprintf(path,"/Client0/ServiceBrowser%i",serv++);
    printf("service %s %s %s \n",sender,dest,path);
    char *p = path;
     
    reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_OBJECT_PATH , &p,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);

    send_signal_service_cache(connection,sender,dest,path);
}

static void respond_to_record(DBusConnection *connection, DBusMessage *request) {
    DBusMessage *reply;
    DBusError error;  
 
    int32_t iface,prot;
    const char *name;
    uint16_t clazz,type;
    uint32_t flags;

    dbus_error_init(&error);    
    dbus_message_get_args(request, &error,
                DBUS_TYPE_INT32, &iface,
                DBUS_TYPE_INT32, &prot,
                DBUS_TYPE_STRING, &name,
                DBUS_TYPE_UINT16, &clazz,
                DBUS_TYPE_UINT16, &type,
                DBUS_TYPE_UINT32, &flags,
                DBUS_TYPE_INVALID);

    if (dbus_error_is_set(&error)) {
        reply = dbus_message_new_error(request, "wrong_arguments", "Illegal arguments");
        dbus_connection_send(connection, reply, NULL);
        dbus_message_unref(reply);
        return;
    }

    printf("record %i,%i,%s,%i,%i,%u\n",iface,prot,name,clazz,type,flags);

    char *ret = "/Client0/RecordBrowser1";
     
    reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                 DBUS_TYPE_OBJECT_PATH , &ret,
                 DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);

    send_signal_record_item(connection,request,"/Client0/RecordBrowser1",0);

    send_signal_record_cache(connection,request,"/Client0/RecordBrowser1");

    const char * send = dbus_message_get_sender(request);
    const char * dest = dbus_message_get_destination(request);

    send_signal_service_all(connection,send,dest,"/Client0/ServiceBrowser2");

    done = 1;

} 

int main() {
	  
    DBusConnection *connection; 
    DBusError error;  
    DBusObjectPathVTable vtable;

    dbus_error_init(&error);  
    connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error); 
    check_and_abort(&error);

    dbus_bus_request_name(connection, "org.freedesktop.Avahi", 
                          DBUS_NAME_FLAG_REPLACE_EXISTING, &error);	  
    check_and_abort(&error);

    vtable.message_function = tutorial_messages;  
    vtable.unregister_function = NULL;

    dbus_connection_try_register_object_path(connection, "/", &vtable, NULL, &error);	  
    check_and_abort(&error);

    while(1) {		  
        dbus_connection_read_write_dispatch(connection, 100); 
         if (serv == 7) {
            printf("answer %s %s\n",sender,dest);
            send_signal_service_all(connection,sender,dest,"/Client0/ServiceBrowser1");
            send_signal_service_item(connection,sender,dest,"/Client0/ServiceBrowser2",0);
            //send_signal_service_item(connection,sender,dest,"/Client0/ServiceBrowser2",1);
            send_signal_service_all(connection,sender,dest,"/Client0/ServiceBrowser3");
            send_signal_service_all(connection,sender,dest,"/Client0/ServiceBrowser4");
            send_signal_service_all(connection,sender,dest,"/Client0/ServiceBrowser5");
            send_signal_service_all(connection,sender,dest,"/Client0/ServiceBrowser6");
            serv++;
         }
         if (done == 1) {
             printf("done\n");
             //send_signal_service_all(connection,sender,dest,"/Client0/ServiceBrowser2");
         }
    }

    return 0;
}