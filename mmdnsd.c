/*
 * tinysvcmdns - a tiny MDNS implementation for publishing services
 * Copyright (C) 2011 Darell Tan
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE   //csto

#include <netdb.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <ifaddrs.h>
#include "mdns.h"
#include "mdnsd.h"
#include "hashtable.h"
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#ifdef NSS
#include "nsssocket.h"
#endif

#ifdef DBUS
#include "dbusconn.h"
#endif


struct hostinfo {
	char* name;
	char* iface;
	uint32_t ttl;
	uint32_t v4addr;
	struct in6_addr v6addr;
	struct rr_entry *a_e;
	struct rr_entry *aaaa_e;
};

static struct hostinfo *parse_args(int argc, char* argv[]) {
	char c;

	struct hostinfo *hinfo = malloc(sizeof(struct hostinfo));
    memset(hinfo,0,sizeof(struct hostinfo));

	hinfo->ttl = 120;	// default check every ttl seconds for new info


	while ((c = getopt (argc, argv, "t:i:")) != -1) {
		switch (c) {
		case 't':
			hinfo->ttl = atoi(optarg);
			break;
		case 'i':
			hinfo->iface = optarg;
			break;
		case '?':
			if (optopt == 'x')
			fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			else
			fprintf (stderr,
					"Unknown option character `\\x%x'.\n",
					optopt);
		default:
			exit(1);
		}
	}
	printf("options: %i %s \n",hinfo->ttl,hinfo->iface);

	return hinfo;

}

static struct hostinfo *update_hostinfo(struct mdnsd *svr, struct hostinfo *hinfo) {

	char hostname[256];
	uint32_t v4addr = 0;
	struct in6_addr v6addr = IN6ADDR_ANY_INIT;

	gethostname(hostname,256);
	strcat(hostname,".local");

	hinfo->name = hostname;

	// get ip adresses of interfaces
	struct ifaddrs *ifaddr, *ifa;

	if (getifaddrs(&ifaddr) == -1) {
		perror("getifaddrs");
		exit(1); // no interfaces to serve
	}

    //Walk through list, maintaining head pointer so we can free
	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {

		if (strcmp(ifa->ifa_name,"lo")==0) continue; //skip loopback interface
        if (hinfo->iface != NULL) {                   //if given, check hinfo->iface
            printf("asdf %s %s\n",ifa->ifa_name,hinfo->iface);
            if (strcmp(ifa->ifa_name,hinfo->iface) != 0) { 
                continue;
            }
        }

		int family = ifa->ifa_addr->sa_family;

      	if (family == AF_INET) {
			char ip_addr[NI_MAXHOST];
			int s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in),
								ip_addr, sizeof(ip_addr), NULL, 0, NI_NUMERICHOST);
			if (s != 0) {
				printf("getnameinfo() failed: %s\n", gai_strerror(s));
			} else {
				printf("%s: %s\n", ifa->ifa_name, ip_addr);
				v4addr = ((struct sockaddr_in*) ifa->ifa_addr)->sin_addr.s_addr;	
			}
		}
		if (family == AF_INET6) {
			char ip_addr[NI_MAXHOST];
			int s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in6),
								ip_addr, sizeof(ip_addr), NULL, 0, NI_NUMERICHOST);
			if (s != 0) {
				printf("getnameinfo() failed: %s\n", gai_strerror(s));
			} else {
				printf("%s: %s\n", ifa->ifa_name, ip_addr);
				v6addr = ((struct sockaddr_in6*) ifa->ifa_addr)->sin6_addr;
				break; //only use the first interface with ip adresses
			}
		}	
	}

	if (v4addr != 0 && v4addr != hinfo->v4addr) { 
		if (hinfo->name == NULL) { 
			mdnsd_set_hostname(svr, hostname, v4addr);
			hinfo->name = hostname;
		}

		printf("v4addr: %s %x\n", ifa->ifa_name, v4addr);

		if (hinfo->a_e) {
			mdnsd_remove_rr(svr,hinfo->a_e);
		}
		struct rr_entry *a_e = rr_create_a(create_nlabel(hostname), v4addr);
		mdnsd_add_rr(svr, a_e);
		hinfo->v4addr = v4addr;
		hinfo->a_e = a_e;
	}

	if (!IN6_IS_ADDR_UNSPECIFIED(&v6addr) && !IN6_ARE_ADDR_EQUAL(&v6addr,&hinfo->v6addr)) { 

		uint64_t* a = (uint64_t*) &v6addr;      //code to print ip6 addr
	    printf("v6addr %s %lx %lx \n",ifa->ifa_name,a[0],a[1]);

		if (hinfo->aaaa_e) {
			mdnsd_remove_rr(svr,hinfo->aaaa_e);
		}
		struct rr_entry *aaaa_e = rr_create_aaaa(create_nlabel(hostname), v6addr);
		mdnsd_add_rr(svr, aaaa_e);
		hinfo->v6addr = v6addr;
		hinfo->aaaa_e = aaaa_e;
	}

	// cleanup
	freeifaddrs(ifaddr);

    if (hinfo->v4addr == 0 && IN6_IS_ADDR_UNSPECIFIED(&v6addr) ) {
        perror("No IP addresses found\n");
        exit(1);
    }

	return hinfo;
}

static int open_netlink_socket() {
	struct sockaddr_nl addr;
    int sock, len;
    char buffer[4096];
    struct nlmsghdr *nlh;

    if ((sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) == -1) {
        perror("couldn't open NETLINK_ROUTE socket");
        return 1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.nl_family = AF_NETLINK;
    addr.nl_groups = RTMGRP_IPV4_IFADDR;

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("couldn't bind");
        return 1;
    }

	return sock;
}


int main(int argc, char *argv[]) {

	char buf[128];
	
	//start server
	struct mdnsd *svr = mdnsd_start();
	if (svr == NULL) {
		printf("mdnsd_start() error\n");
		return 1;
	}

	// parse arguments (allocates hinfo) and update name/ip addresses
	struct hostinfo *hinfo = parse_args(argc,argv);
	hinfo = update_hostinfo(svr,hinfo);

	// const char *txt[] = {
	// 	"path=/mywebsite", 
	// 	NULL
	// };
	// struct mdns_service *svc = mdnsd_register_svc(svr, "My Website", 
	// 								"_http._tcp.local", 8080, NULL, txt);
	// mdns_service_destroy(svc);

	// open netlink socket and allocate memory for data
	int netlink = open_netlink_socket();
    struct nlmsghdr *nlh = malloc(4096);

	int fd_max;
	fd_set sockfd_set,read_set;
	FD_ZERO(&sockfd_set);
	FD_SET(netlink,&sockfd_set);
	FD_SET(svr->packet_pipe[0], &sockfd_set);
	fd_max = svr->packet_pipe[0];

#ifdef NSS
	// create thread to take nss queries
	struct nsss *nss = nss_socket();
	FD_SET(nss->nss_pipe[0], &sockfd_set);
	fd_max = nss->nss_pipe[0];
#endif

#ifdef DBUS
	struct dbus_conn *dbusc = dbus_connect();
	FD_SET(dbusc->dbus_pipe[0], &sockfd_set);
	fd_max = dbusc->dbus_pipe[0];
#endif

	printf("added service and hostname. press ctrl-c to exit\n");

    int nss_pending = 0;
	struct timeval tv = {hinfo->ttl, 0};   
	while (1) {
		read_set = sockfd_set;
		select(fd_max+1, &read_set, NULL, NULL, &tv);

		if (FD_ISSET(svr->packet_pipe[0], &read_set)) {   //Packet arrived
			read(svr->packet_pipe[0],buf,1);	//clear pipe 

			struct mdns_pkt *pkt = mdnsd_get_packet(); //get packet from queue
			if (!pkt) continue;

			printf("Packet qn: %i an: %i \n",pkt->num_qn,pkt->num_ans_rr);
	
			if (pkt->num_qn > 0) {					 // process mdns queries 
				mdnsd_process_query(svr,pkt);
			}		
#ifdef NSS
			if (nss_pending && pkt->num_ans_rr > 0) { // process nss queries  
				nss_pending = nss_process_query(svr, nss, pkt);
			}
#endif //NSS	
#ifdef DBUS
			if (dbusc->working && pkt->num_ans_rr > 0 ) { // process dbus queries 
				if (pkt->rr_ans->e->type == RR_PTR) {
					dbus_process_query(dbusc,pkt);
				}
			}
#endif //DBUS
			mdns_pkt_destroy(pkt); // done with the packet
#ifdef NSS
		} else if (FD_ISSET(nss->nss_pipe[0], &read_set)) {
			read(nss->nss_pipe[0],buf,1);				//clear pipe 
			printf("NSS question %i %s\n",nss->query->type,nss->query->name);
			nss_pending = nss_process_query(svr, nss, NULL);
			if (nss_pending)
				mdnsd_add_qn(svr,nss->query->name,nss->query->type);
#endif //NSS
		} else if (FD_ISSET(netlink,&read_set)) {			  //network change
			int len = read(netlink, nlh, 4096);
			printf("Netlink\n");
			while ((NLMSG_OK(nlh, len)) && (nlh->nlmsg_type != NLMSG_DONE)) {
				if (nlh->nlmsg_type == RTM_NEWADDR) {
					hinfo = update_hostinfo(svr,hinfo); 
				}
            	nlh = NLMSG_NEXT(nlh, len);
        	}
#ifdef DBUS
		}   else if (FD_ISSET(dbusc->dbus_pipe[0],&read_set)) {
			read(dbusc->dbus_pipe[0],buf,1);	
			printf("DBus %s\n",dbusc->query->name);
			mdnsd_add_qn(svr,dbusc->query->name,RR_PTR);
			tv.tv_sec = 2; tv.tv_usec = 0;
			dbusc->working = 1;
#endif
		} else {			
			struct timespec t;    
			clock_gettime(CLOCK_MONOTONIC,&t);
			printf("Timeout %i\n",t.tv_sec);

#ifdef DBUS
			if (dbusc->working == 1) {
				dbus_process_done(dbusc);
				dbusc->working = 0;
			}
#endif
			hinfo = update_hostinfo(svr,hinfo);
			tv.tv_sec = hinfo->ttl; tv.tv_usec = 0;
			nss_pending = 0;		
		}	
	}
 
	mdnsd_stop(svr);

	return 0;
}