#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include "mdns.h"

#define SOCKET_PATH "/var/run/avahi-daemon/socket"

struct nss_query {
	char name[256];
	rr_type type;
	char ans[256];
};

struct nsss {
	pthread_mutex_t data_lock;
	int sockfd;
	int nss_pipe[2];
	int ip_pipe[2];

	struct nss_query *query;	

	int stop_flag;
};

struct nsss* nss_socket();

int nss_process_query(struct mdnsd *svr, struct nsss* nss, struct mdns_pkt *pkt);
