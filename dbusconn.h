#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <dbus-1.0/dbus/dbus.h>
#include "mdns.h"

//#define SOCKET_PATH "/var/run/avahi-daemon/socket"

struct dbus_query {
	char name[256];
	rr_type type;
	char ans[256];
};

struct dbus_client {
    DBusConnection *conn;
    char serv[256];
    char clnt[256];
};

struct dbus_conn {
	pthread_mutex_t data_lock;
	int dbus_pipe[2];

	struct dbus_query  *query;

    int working;	
	int services;
	int records;

    DBusConnection *conn; 
    DBusError *err;  

	struct dbus_client *client;

	int stop_flag;
};

struct dbus_conn* dbus_connect();

int dbus_process_query(struct dbus_conn *svr, struct mdns_pkt *pkt);

int dbus_process_done(struct dbus_conn *svr);

