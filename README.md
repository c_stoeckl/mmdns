#A simple mDNS daemon for Linux based on the tinysvcmdns code 
(See README.tinysvcmdns file for details)

Features:

Modular code can be compiled to provide:

    - mDNS responses only
    - mDNS name resolution (name service switch integration)
    - mDNS printer search  (DBUS interface used by CUPS)

    
The build system is CMake

- The code is quite small (40-70 kB)
- It has very few external dependencies (dbus-1, pthread, libm)
- It uses the well known AVAHI socket and DBUS interfaces and 
therefore can not be run in parallel with AVAHI.

Basic functionality has been tested 
The DBUS part is still work in progress. 

Feedback on the project would be very much appreciated. 


