#
# Makefile for tinysvcmdns
#

CFLAGS += -Wall -pedantic -std=gnu99
#CFLAGS += -g
CFLAGS += -O2 -DNDEBUG
LDLIBS = -lpthread -lm

ifneq ($(CROSS_COMPILE),)
  CC = gcc
  CC := $(CROSS_COMPILE)$(CC)
  AR := $(CROSS_COMPILE)$(AR)
endif

BIN=testmdnsd testmdns socket

LIBTINYSVCMDNS_OBJS = mdns.o mdnsd.o hashtable.o nsssocket.o

.PHONY: all clean

all: $(BIN) libtinysvcmdns.a

clean:
	-$(RM) *.o
	-$(RM) *.bin
	-$(RM) mdns
	-$(RM) $(BIN)
	-$(RM) libtinysvcmdns.a

mdns: mdns.o

mdnsd: mdns.o mdnsd.o hashtable.o

testmdnsd: testmdnsd.o libtinysvcmdns.a

testmdns: testmdns.o libtinysvcmdns.a

socket: socket.o hash_table.o

libtinysvcmdns.a: $(patsubst %, libtinysvcmdns.a(%), $(LIBTINYSVCMDNS_OBJS))

